import React from 'react';
import {View, TouchableOpacity, Image, Platform} from 'react-native';
import {Actions} from 'react-native-router-flux';

const styles = {
  image: {
    width: 30,
    height: 30,
    margin: 10,
  },
  panel: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.8,
        shadowRadius: 2,
      },
      android: {
        elevation: 10,
      },
    }),
  },
};

export default class CustomTabBar extends React.Component {
  render() {
    const {state} = this.props.navigation;
    const activeTabIndex = state.index;

    const paths = {
      Home: require('../Images/Home.png'),
      PhotoEditor: require('../Images/PhotoEditor.png'),
      Filter: require('../Images/Filter.png'),
      Settings: require('../Images/Settings.png'),
    };

    return (
      <View style={styles.panel}>
        {state.routes.map(element => (
          <TouchableOpacity
            key={element.key}
            onPress={() => Actions[element.key]()}>
            <Image style={styles.image} source={paths[element.key]} />
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}
