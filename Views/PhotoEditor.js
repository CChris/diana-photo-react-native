import React from 'react';
import {View, Dimensions, TouchableOpacity, Image} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import {connect} from 'react-redux';
import {changeSelection} from '../ApplicationState/Actions';
import {bindActionCreators} from 'redux';

const windowWidth = Dimensions.get('window').width;

const styles = {
  main: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  image: {
    width: windowWidth * 0.85,
    height: windowWidth * 0.85,
    borderRadius: 25,
  },
  imageInfo: {
    color: 'white',
  },
};

class PhotoEditor extends React.Component {
  onPress = (uri, index) => {
    if (uri === '') {
      return;
    }

    ImagePicker.openCropper({
      path: uri,
      width: 500,
      height: 500,
    })
      .then(image => {
        this.props.changeSelection(image.path, index);
      })
      .catch(r => console.log(r));
  };

  render() {
    return (
      <View style={styles.main}>
        <TouchableOpacity
          onPress={() => this.onPress(this.props.selections[0].uri, 0)}>
          <Image
            style={styles.image}
            source={
              this.props.selections[0].uri === ''
                ? require('../Images/gradientTest.png')
                : {uri: this.props.selections[0].uri}
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.onPress(this.props.selections[1].uri, 1)}>
          <Image
            style={styles.image}
            source={
              this.props.selections[1].uri === ''
                ? require('../Images/gradientTest2.png')
                : {uri: this.props.selections[1].uri}
            }
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const {selections} = state;
  return {selections};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changeSelection,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotoEditor);
