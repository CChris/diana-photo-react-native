import React from 'react';
import {View} from 'react-native';

const styles = {
  main: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

class Settings extends React.Component {
  render() {
    return <View style={styles.main} />;
  }
}

export default Settings;
