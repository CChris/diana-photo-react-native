import React from 'react';
import {
  View,
  Animated,
  TouchableOpacity,
  Dimensions,
  Image,
  FlatList,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import SlidingUpPanel from 'rn-sliding-up-panel';
import CameraRoll from '@react-native-community/cameraroll';
import {connect} from 'react-redux';
import {changeSelection} from '../ApplicationState/Actions';
import {bindActionCreators} from 'redux';

const windowWidth = Dimensions.get('window').width;

const styles = {
  panel: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,

    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.8,
        shadowRadius: 2,
      },
      android: {
        elevation: 10,
      },
    }),
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#212121',
    justifyContent: 'center',
  },
  text: {
    color: 'white',
  },
  image: {
    width: windowWidth / 4.3,
    height: windowWidth / 4.3,
    margin: (windowWidth - (windowWidth / 4.3) * 4) / 10,
  },
  selectedImage: {
    opacity: 0.2,
  },
  header: {
    width: '100%',
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerToggle: {
    width: 100,
    height: 10,
    backgroundColor: '#e1e1e1',
    borderRadius: 5,
  },
  headIcon: {
    position: 'absolute',
    top: -windowWidth / 2,
    width: windowWidth,
    height: windowWidth,
    zIndex: 1,
  },
  footer: {
    height: 150,
    width: '100%',
    backgroundColor: 'white',
  },
};

class Home extends React.Component {
  _draggedValue = new Animated.Value(25);
  _draggableRange = {top: 600, bottom: 25};

  constructor(props) {
    super(props);
    this.state = {
      data: '',
      lastIndexes: [-1, -1],
    };
  }

  async componentDidMount() {
    if (Platform.OS === 'android') {
      const result = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'User Permission',
          message: 'Application requires access to your gallery!',
        },
      );
      if (result !== 'granted') {
        return;
      }
    }

    CameraRoll.getPhotos({
      first: 50,
      assetType: 'Photos',
    })
      .then(res => {
        let random = {node: {image: {local: '../Images/dices.png'}}};
        let collection = [];
        collection.push(random);
        collection = collection.concat(res.edges);

        for (let i = 0; i < collection.length; i++) {
          collection[i].node.image.selectedStyle = styles.image;
        }

        this.setState({data: collection});
      })
      .catch(error => {
        console.log(error);
      });
  }

  onPress = (item, index) => {
    if (index === 0) {
      this.randomizeUris();
      return;
    }

    if (this.state.lastIndexes.includes(index)) {
      this.deselectUri(index);
    } else {
      this.selectUri(index);
    }

    this.updatePhotoEditor();
  };

  updatePhotoEditor() {
    if (this.state.lastIndexes[0] !== -1) {
      this.props.changeSelection(
        this.state.data[this.state.lastIndexes[0]].node.image.uri,
        0,
      );
    } else {
      this.props.changeSelection('', 0);
    }
    if (this.state.lastIndexes[1] !== -1) {
      this.props.changeSelection(
        this.state.data[this.state.lastIndexes[1]].node.image.uri,
        1,
      );
    } else {
      this.props.changeSelection('', 1);
    }
  }

  selectUri(index) {
    let source = this.state.data;
    let newIndexes = this.state.lastIndexes;
    source[index].node.image.selectedStyle = styles.selectedImage;

    if (newIndexes.includes(-1)) {
      if (newIndexes[0] === -1) {
        newIndexes[0] = index;
      } else {
        newIndexes[1] = index;
      }
    } else {
      this.deselectUri(newIndexes[0]);
      this.selectUri(index);
    }

    this.setState({data: source, lastIndexes: newIndexes});
  }

  deselectUri(index) {
    let source = this.state.data;
    let newIndexes = this.state.lastIndexes;
    source[index].node.image.selectedStyle = styles.image;

    if (newIndexes[0] === index) {
      newIndexes[0] = newIndexes[1];
    }

    newIndexes[1] = -1;

    this.setState({data: source, lastIndexes: newIndexes});
  }

  randomizeUris() {
    if (this.state.data.length < 4) {
      return;
    }

    let a = 0;

    do {
      a = Math.floor(Math.random() * this.state.data.length);
    } while (a === 0);

    let b = 0;

    do {
      b = Math.floor(Math.random() * this.state.data.length);
    } while (b === 0 || b === a);

    this.selectUri(a);
    this.selectUri(b);

    this.updatePhotoEditor();
  }

  render() {
    const scale = this._draggedValue.interpolate({
      inputRange: [130, 600],
      outputRange: [1, 0.3],
      extrapolate: 'clamp',
    });

    return (
      <View style={styles.panel}>
        <SlidingUpPanel
          ref={c => (this._panel = c)}
          animatedValue={this._draggedValue}
          draggableRange={this._draggableRange}>
          <View style={styles.container}>
            <Animated.View
              style={[
                styles.headIcon,
                {
                  transform: [{scale: scale}],
                },
              ]}>
              <Image
                style={styles.headIcon}
                source={require('../Images/man.png')}
              />
            </Animated.View>
            <View style={styles.header}>
              <View style={styles.headerToggle} />
            </View>
            <FlatList
              data={this.state.data}
              numColumns={4}
              renderItem={({item, index}) => (
                <TouchableOpacity onPress={() => this.onPress(item, index)}>
                  <Image
                    style={[styles.image, item.node.image.selectedStyle]}
                    source={
                      'uri' in item.node.image
                        ? {uri: item.node.image.uri}
                        : require('../Images/dices.png')
                    }
                  />
                </TouchableOpacity>
              )}
              keyExtractor={(item, index) => index}
            />
            <View style={styles.footer} />
          </View>
        </SlidingUpPanel>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const {selections} = state;
  return {selections};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changeSelection,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
