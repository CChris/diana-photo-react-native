import React from 'react';
import {
  Image,
  View,
  Dimensions,
  TouchableOpacity,
  Platform,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';

const windowWidth = Dimensions.get('window').width;

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
  },
  main: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  resultImage: {
    width: windowWidth * 0.85,
    height: windowWidth * 0.85,
    borderRadius: 25,
  },
  filtersPanel: {
    width: '100%',
    height: 70,
    backgroundColor: 'white',
    alignItems: 'center',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.8,
        shadowRadius: 2,
      },
      android: {
        elevation: 10,
      },
    }),
  },
  filterImage: {
    width: 50,
    height: 50,
    margin: 10,
  },
};

class Filter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        require('../Images/Filters/f_gradient.png'),
        require('../Images/Filters/f_blur.png'),
        require('../Images/Filter.png'),
        require('../Images/Filter.png'),
        require('../Images/Filter.png'),
        require('../Images/Filter.png'),
      ],
    };
  }

  onPress = index => {
    switch (index) {
      case 0:
        console.log('gradient');
        break;
      case 1:
        console.log('blur');
        break;
      default:
        console.log('not implemented yet :(');
        break;
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.main}>
          <Image
            style={styles.resultImage}
            source={
              this.props.selections[2].uri === ''
                ? require('../Images/gradientTest2.png')
                : {uri: this.props.selections[2].uri}
            }
          />
        </View>
        <View style={styles.filtersPanel}>
          <FlatList
            data={this.state.data}
            horizontal={true}
            renderItem={({item, index}) => (
              <TouchableOpacity onPress={() => this.onPress(index)}>
                <Image style={styles.filterImage} source={item} />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const {selections} = state;
  return {selections};
};

export default connect(mapStateToProps)(Filter);
