import React, {Component} from 'react';
import Home from './Views/Home';
import {Scene, Router, Tabs} from 'react-native-router-flux';
import PhotoEditor from './Views/PhotoEditor';
import TabNavigationBar from './Components/TabNavigationBar';
import Settings from './Views/Settings';
import Filter from './Views/Filter';
import {createStore} from 'redux';
import selectionsState from './ApplicationState/Reducers';
import {Provider} from 'react-redux';

const store = createStore(selectionsState);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Tabs tabBarComponent={TabNavigationBar}>
            <Scene hideNavBar key="Home" component={Home} />
            <Scene hideNavBar key="PhotoEditor" component={PhotoEditor} />
            <Scene hideNavBar key="Filter" component={Filter} />
            <Scene hideNavBar key="Settings" component={Settings} />
          </Tabs>
        </Router>
      </Provider>
    );
  }
}

export default App;
