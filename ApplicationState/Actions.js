export const CHANGE_SELECTION = 'CHANGE_SELECTION';
export const ADD_SELECTION = 'ADD_SELECTION';

export function changeSelection(uri, index) {
  return {type: CHANGE_SELECTION, uri, index};
}

export function addSelection(uri) {
  return {type: ADD_SELECTION, uri};
}
