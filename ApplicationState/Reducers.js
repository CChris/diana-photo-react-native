import {combineReducers} from 'redux';
import {CHANGE_SELECTION, ADD_SELECTION} from './Actions';

const INITIAL_STATE = [{uri: ''}, {uri: ''}, {uri: ''}];

function selections(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CHANGE_SELECTION:
      return state.map((selection, index) => {
        if (index === action.index) {
          return Object.assign({}, selection, {
            uri: action.uri,
          });
        }
        return selection;
      });
    case ADD_SELECTION:
      return [
        ...state,
        {
          uri: action.uri,
        },
      ];
    default:
      return state;
  }
}

const selectionsState = combineReducers({
  selections,
});

export default selectionsState;
